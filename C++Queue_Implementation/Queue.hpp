/*==========================================
	Queue.hpp
	author: Joshua Wright
	contact: joshuawright1197@gmail.com
	Date: 2016-12-03
	description : Queue implementation 
===========================================*/

#pragma once
#include <memory>

template<class T>
class Queue
{
public:
	using value_type			= T;
	using value_pointer_type	= T*;
	using value_reference_type	= T&;

private:
	struct node {
		value_type				value_;
		std::shared_ptr<node>	inFrontNode_;
		std::shared_ptr<node>	behindNode_;
	};
	std::shared_ptr<node> front_;
	std::shared_ptr<node> back_;
	size_t size_;
public:
	Queue(): size_(0),back_(new node), front_(new node) {}

	Queue(Queue const&) = default;
	Queue& operator= (Queue const&) = default;

	value_type front() { return front_->value_; }
	value_type back() { 
		if (size_ != 1)
			return back_->value_;
		return front_->value_;
	}
	size_t size() { return size_; }
	bool empty() { return size_ == 0; }

	void push(value_type const& value) {
		++size_;

		switch (size_) {
		case 1:
			back_->inFrontNode_ = front_;
			back_->behindNode_ = nullptr;
			front_->value_ = value;
			front_->inFrontNode_ = nullptr;
			front_->behindNode_ = back_;
			break;
		case 2:
			back_->value_ = value;
			back_->inFrontNode_ = front_;
			back_->behindNode_ = nullptr;
			break;
		default:
			std::shared_ptr<node> newBack(new node);
			newBack->value_ = value;
			newBack->inFrontNode_ = back_;
			newBack->behindNode_ = nullptr;
			back_->behindNode_ = newBack;
			back_ = newBack;
		}
	}
	value_type pop() {
		--size_;
		value_type result = front_->value_;	
		front_->inFrontNode_ = nullptr;
		front_ = front_->behindNode_;
		return result;
	}

	void clear() {
		while (!empty()) 
			pop();	
	}

	~Queue() {
		clear();
	}
};