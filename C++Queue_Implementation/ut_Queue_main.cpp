/*==========================================
ut_Queue_main.cpp
author: Joshua Wright
contact: joshuawright1197@gmail.com
Date: 2016-12-03
description : Queue Unit test
============================================*/

#define BOOST_TEST_MODULE QueueTest
#define FULL_TEST true

#include <boost\test\unit_test.hpp>
#include <iostream>
#include <functional>
#include "Queue.hpp"
#include <string>
using namespace std;


BOOST_AUTO_TEST_CASE(intro) {
	cout << "\nQueue Unit Test\n";
	cout << "Last compiled: " << __TIMESTAMP__ << "\n\n";
}

BOOST_AUTO_TEST_CASE(Queue_constructor_test) {
	Queue<int> q;
	BOOST_CHECK(q.size() == 0);
	BOOST_CHECK(q.empty() == true);
}

BOOST_AUTO_TEST_CASE(Queue_push_clear_test) {
	Queue<int> q;
	q.push(65);
	BOOST_CHECK(q.size() == 1);

	q.push(25);
	q.push(165);
	BOOST_CHECK(q.size() == 3);

	BOOST_CHECK(q.empty() == false);

	q.clear();
	BOOST_CHECK(q.empty() == true);
}

BOOST_AUTO_TEST_CASE(Queue_front_back_test) {
	Queue<int> q;
	q.push(23);
	BOOST_CHECK(q.front() == 23);
	BOOST_CHECK(q.back() == 23);

	q.push(54);
	BOOST_CHECK(q.front() == 23);
	BOOST_CHECK(q.back() == 54);

	q.push(87);
	BOOST_CHECK(q.back() == 87);
}


#if FULL_TEST
BOOST_AUTO_TEST_CASE(Stack_full_test) {
	Queue<int> q;
	int value;
	function<int(size_t)> make_value = [](size_t index)->int {
		return index * 10;
	};

	for (size_t i = 1; i <= 15000; ++i) {
		value = make_value(i);
		q.push(value);
		BOOST_CHECK(q.back() == value);
		BOOST_CHECK(q.size() == i);
		BOOST_CHECK(q.front() == 10);
	}
	for (size_t i = 1; i <= 15000; ++i) {
		value = make_value(i);
		BOOST_CHECK(q.pop() == make_value(i));
	}
}
#endif 